<?php

namespace Belvg\CustomAPI\Api;

interface TestInterface
{
    /**
     * Test function
     *
     * @api
     * @return string
     */
    public function test();

    /**
     * Test function
     *
     * @api
     * @param string $param
     * @return string
     */
    public function test1($param);
}