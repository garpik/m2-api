<?php

namespace Belvg\CustomAPI\Model;
class Test implements \Belvg\CustomAPI\Api\TestInterface
{
    /**
     * Test function
     *
     * @api
     * @return string
     */
    public function test()
    {
        return 'test';
    }

    /**
     * Test function
     *
     * @api
     * @param string $param
     * @return string
     */
    public function test1($param)
    {
        return $param;
    }
}